This is the repository for old wormsim versions. At present there are several versions available. For an overview, go to the main page of this project, and at the bottom of the page you will see a listing. You can also reach this in the main gitlab menu via Repository > main. 
Here you can click under the individual files listed under "name", and then choose to download them.  

A backup of the original wormsim (and other) repositories can be found at:
\\storage.erasmusmc.nl\x\avcl13\MAGE\DATA\DataArchive\2021\2021010

Due to bad version management in the past, source code is only available to a very limited number of versions: 2.58Ap59 and 2.71. 
If you want the source code of a particular version other than those, you will have to decompile. This can be done with a java decompiler like http://java-decompiler.github.io/. 
